# Generated by Django 4.2.3 on 2023-07-18 21:47

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0003_alter_todolist_name_todoitem"),
    ]

    operations = [
        migrations.AlterField(
            model_name="todoitem",
            name="due_date",
            field=models.DateField(
                blank=True, default=django.utils.timezone.now
            ),
            preserve_default=False,
        ),
    ]
