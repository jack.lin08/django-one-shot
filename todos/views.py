from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoForm, TodoItemForm

# Create your views here.


def my_todo_list(request):
    todolists = TodoList.objects.all()
    context = {
        "todo_list": todolists,
    }

    return render(request, "todos/my_list.html", context)


def todo_item(request, id):
    todoitem = get_object_or_404(TodoList, id=id)
    context = {
        "todoitem": todoitem,
    }
    return render(request, "todos/todo_list_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_item", id=item.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_item", id=item.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/itemcreate.html", context)


def todo_list_edit(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo)
        if form.is_valid:
            form.save()
            return redirect("todo_item", id)
    else:
        form = TodoForm(instance=todo)
    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_item_update(request, id):
    todo = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo)
        if form.is_valid:
            item = form.save()
            return redirect("todo_item", item.list.id)
    else:
        form = TodoItemForm(instance=todo)
    context = {
        "form": form,
    }
    return render(request, "todos/itemedit.html", context)


def todo_list_delete(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("my_todo_list")
    else:
        form = TodoForm(instance=todo)
    context = {
        "form": form,
    }
    return render(request, "todos/delete.html", context)
