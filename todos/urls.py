from .views import (
    my_todo_list,
    todo_item,
    todo_list_create,
    todo_list_edit,
    todo_list_delete,
    todo_item_create,
    todo_item_update,
)
from django.urls import path

urlpatterns = [
    path("", my_todo_list, name="my_todo_list"),
    path("<int:id>/", todo_item, name="todo_item"),
    path("create/", todo_list_create, name="todo_list_create"),
    path("<int:id>/edit/", todo_list_edit, name="todo_list_edit"),
    path("<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
    path("items/create", todo_item_create, name="todo_item_create"),
    path("items/<int:id>/edit/", todo_item_update, name="todo_item_update"),
]
